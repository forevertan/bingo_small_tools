# bingo_small_tools

#### 介绍
1.  ws2812取字模小工具  
    适用ws2812b_16x16点阵
2.  乐鑫模组量产烧录工具  
    1) LyricBox  
       歌词音箱固件，适用乐鑫全系列ESP32 WiFi&BT双模模组。  
    2) Screen_Chase_Light  
       流光溢彩随屏灯固件，适用于全系列ESP32标准模组。 
    3) WLED  
       1）ESP32  
          ESP32_WLED固件，适用于全系列ESP32标准模组。  
       2) ESP8266  
          ESP8266_WLED固件，适用于ESP8266_12F等模组。  

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
